#ifndef _PLAYSCREEN_H_
#define _PLAYSCREEN_H_
#include <AMUI/ListBox.h>
#include <AMUI/Image.h>
#include <MTXml/MTXml.h>
#include <AMUI/Widget.h>
#include <AMXml/XMLDocument.h>
#include <AMXml/XMLReader.h>
#include <AMXml/XMLWriter.h>
#include <AMXml/XMLNode.h>
#include <AMXml/XPath.h>
#include "CategoryScreen.h"
#include "MainScreen.h"
#include <AMUI/Screen.h>
//#include "MyBadgesScreen.h"
#include <AMUI/StackLayout.h>

using namespace MAUtil;
//using namespace MAUI;
using namespace AMXml;

class PlayScreen : public AMUI::Screen, AMUI::WidgetListener{
public:
        PlayScreen(AMUI::Screen *previous, String my_level);
        ~PlayScreen();
        void keyPressEvent(int keyCode, int nativeCode);
        void pointerPressEvent(MAPoint2d point);
        void pointerReleaseEvent(MAPoint2d point);
        String LoadTrivia(int new_position, String triviapart);

        void selectPreviousItem();
        void selectNextItem();
        void buttonAction();
        void processAnswer();
        void show();
        void hide();

private:
        Vector<Screen*> screens;
        AMUI::Screen *previous;
        AMUI::ListBox *optionlist;
        AMUI::ListBox * answer_back_list;
        AMUI::StackLayout *layout;
        AMUI::Layout *listspacer;
        AMUI::Layout *nextprev_layout;
        AMUI::Layout *check_answer;
        AMUI::Image* background;
        AMUI::Label* question;
        AMUI::Label* option1;
        AMUI::Label* option2;
        AMUI::Label* option3;
        AMUI::Label* option4;
        AMUI::Image* next;
        AMUI::Image* b_previous;
        AMUI::Image* answer;
       // Image* back_button;
        AMUI::Label* quit;
        AMUI::Image* skip;

        AMUI::Label* back_button;
        AMUI::Label* final_answer;

        AMUI::Font* font;
        AMUI::Font* smallfont;
        AMUI::WidgetSkin *button_skin;
        AMUI::WidgetSkin * checkbox_skin;
        AMUI::WidgetSkin * bottom_buttons_skin;

        Label* bglabel1;
        AMUI::Label* bglabel2;
        AMUI::Label* bglabel3;
        AMUI::Label* bglabel4;
        AMUI::Label* QNumber_Label;

        XmlReader reader;
        XmlDocument *doc;
        String current_xml;
        XmlNode*  current_trivia;

        int new_position;

        String Uid;
        String Cquestion;
        String opt1;
        String opt2;
        String opt3;
        String opt4;
        String Canswer;
        char* QNumber;

        String current_part;
        String to_return;
        char cBuffer[256];

        String level;
        String e;
        String m;
        String h;
        String p;
       CategoryScreen* scr;
};

#endif  //_PLAYSCREEN_H_
