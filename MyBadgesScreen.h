#ifndef _LAYOUTSCREEN_H_
#define _LAYOUTSCREEN_H_

#include <AMUI/Screen.h>
#include <AMUI/Layout.h>
#include <AMUI/Image.h>
#include <AMUI/ListBox.h>
#include <MAUI/Widget.h>
#include <AMUI/Label.h>
#include <ma.h>
#include "Util.h"
#include "MAHeaders.h"
#include "PlayScreen.h"
#include "MainScreen.h"
#include <MAUtil/Connection.h>
#define CONNECTION_BUFFER_SIZE 1024

//using namespace MAUI;
using namespace MAUtil;

class MyBadgesScreen : public AMUI::Screen,  AMUI::WidgetListener, HttpConnectionListener{
public:
        MyBadgesScreen(AMUI::Screen *previous, String the_level);
        ~MyBadgesScreen();
        void keyPressEvent(int keyCode, int nativeCode);
        void pointerPressEvent(MAPoint2d point);
        void pointerReleaseEvent(MAPoint2d point);
          void PostFame(String data);
          void writeStore(const String& name, const String& data);
          String  readStore(const String& name);
          void checkBadge2Delete();
          void deleteStore(const String& name);

      	void buttonAction();

      void connectFinished(Connection* conn, int result);
      void connRecvFinished(Connection* conn, int result);
      void connWriteFinished(Connection* conn, int result);
      void connReadFinished(Connection* conn, int result);
     // void httpFinished(MAUtil::HttpConnection*, int){}
      void httpFinished(HttpConnection *conn, int result);


private:
      	Vector<Screen*> screens;
      	AMUI::Screen *previous;
      	AMUI::Layout *mainLayout;
      	AMUI::Widget *softKeys;
      	AMUI::ListBox *listBox;
      	AMUI::Image *image;
        AMUI::Layout *badge1_layout;//
        AMUI::Layout *badge2_layout;//
        AMUI::Image* image1;//
        AMUI::Image* image2;//
        AMUI::Image* image3;//
        AMUI::Image* image4;//
        AMUI::Image* image5;//
        AMUI::Image* image6;//
        AMUI::Image* badgeSrcBackGround;
        AMUI::Image* skip;//
        AMUI::Image* next;//
        AMUI::WidgetSkin* badgeSkin;//
        AMUI::WidgetSkin* button_skin;
        AMUI::Font* badge_font;
        AMUI::ListBox* badge_list;
        AMUI::Label* back;
        AMUI::Label* exit;
        AMUI::Label* reset;

        HttpConnection mHttp;
        String mData;
        char buffer[256];
        char mBuffer[CONNECTION_BUFFER_SIZE];
        bool mIsConnected;
        PlayScreen* scr;
        String level;
        MainScreen* scrn;


};

#endif  //_LAYOUTSCREEN_H_
