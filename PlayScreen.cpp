#include "Util.h"
#include "CategoryScreen.h"
#include "PlayScreen.h"
#include <MTXml/MTXml.h>
#include <conprint.h>
#include <AMUI/Widget.h>
#include "MyBadgesScreen.h"
// Include library for random number generator.
#include <mastdlib.h>
// Include library for local time.
#include <maapi.h>
#include <ResCompiler/ResCompiler.h>
#include <AMUI/StackLayout.h>

using namespace MAUtil;
//using namespace MAUI;
using namespace AMXml;


PlayScreen::PlayScreen(AMUI::Screen *previous, String my_level): previous(previous){

        loadResource(PLAY_IMAGE);
        loadResource(RES_FONT);
        loadResource(RES_FONT_SMALLER);
        loadResource(RES_CHECKBOX_CHECKED);
		loadResource(RES_CHECKBOX_UNCHECKED);
		loadResource(RES_BUTTON_ACTIVE);
		loadResource(RES_BUTTON_INACTIVE);

		layout=new AMUI::StackLayout(0, 0, scrWidth, scrHeight, NULL, AMUI::StackLayout::SL_VERTICAL, AMUI::StackLayout::SL_STRETCH);

        background = new AMUI::Image(0, 0, scrWidth, scrHeight, NULL, false, false, PLAY_IMAGE);
	//	background->setAllowResize(AMUI::Widget::ALLOW_RESIZE_NONE);
		background->setScaleImage(true);

		level=my_level;

        lprintfln("Show start:%s",level.c_str());

        e="easy";
        m="medium";
        h="hard";
        p="pro";

		if(level==e){
			lprintfln("Easy:%s",level.c_str());
				loadResource(EASY);
				int size = maGetDataSize(EASY);
				char buffer[size+1];
				buffer[size] = 0;
				maReadData(EASY, buffer, 0, size);
				current_xml = buffer;
		}
		else if(level==m){
			lprintfln("Med:%s",level.c_str());
				loadResource(MEDIUM);
				int size = maGetDataSize(MEDIUM);
				char buffer[size+1];
				buffer[size] = 0;
				maReadData(MEDIUM, buffer, 0, size);
				current_xml = buffer;
		}
		else if(level==h){
				loadResource(HARD);
				int size = maGetDataSize(HARD);
				char buffer[size+1];
				buffer[size] = 0;
				maReadData(HARD, buffer, 0, size);
				current_xml = buffer;
		}
		else if(level==p){
				loadResource(PRO);
				int size = maGetDataSize(PRO);
				char buffer[size+1];
				buffer[size] = 0;
				maReadData(PRO, buffer, 0, size);
				current_xml = buffer;
		}
/*
		else if{
				lprintfln("EASY..No option");
				loadResource(EASY);
				int size = maGetDataSize(EASY);
				char buffer[size+1];
				buffer[size] = 0;
				maReadData(EASY, buffer, 0, size);
				current_xml = buffer;
		}
*/
                XmlReader reader;
                reader.setEncoding(XmlReader::UTF8);
                doc = reader.parseXML(current_xml);
        //      Vector<XmlNode*>  alltrivia = doc->rootNode->find("trivia");

                int progress=0;
                new_position=0;
                QNumber="1/15";
                Uid=LoadTrivia(new_position, "id");
                Cquestion=LoadTrivia(new_position, "question");
                opt1=LoadTrivia(new_position, "option1");
                opt2=LoadTrivia(new_position, "option2");
                opt3=LoadTrivia(new_position, "option3");
                opt4=LoadTrivia(new_position, "option4");
                Canswer=LoadTrivia(new_position, "answer");

        checkbox_skin = new AMUI::WidgetSkin(
                        RES_CHECKBOX_CHECKED,   // AMUI::Image for selected state.
                        RES_CHECKBOX_UNCHECKED, // AMUI::Image for unselected state.
                                0, // X coordinate for start of center patch.
                                0, // X coordinate for end of center patch.
                        0, // Y coordinate for start of center patch.
                        0, // Y coordinate for end of center patch.
                        true,  // Is selected image transparent?
                        true); // Is unselected image transparent?

        bottom_buttons_skin = new AMUI::WidgetSkin(
                        RES_BUTTON_ACTIVE,   // AMUI::Image for selected state.
                        RES_BUTTON_INACTIVE, // AMUI::Image for unselected state.
                                0, // X coordinate for start of center patch.
                                0, // X coordinate for end of center patch.
                        0, // Y coordinate for start of center patch.
                        0, // Y coordinate for end of center patch.
                        true,  // Is selected image transparent?
                        true); // Is unselected image transparent?
        background->setSkin(gSkin);
       // background->setDrawBackground(false);

        font = new AMUI::Font(RES_FONT);
        smallfont = new AMUI::Font(RES_FONT_SMALLER);

        		question = new AMUI::Label(15,20,scrWidth-30,scrHeight/4+10,background,Cquestion,0,smallfont);
                question->setSkin(NULL);
                question->setMultiLine(true);
                question->setDrawBackground(false);

                    optionlist=new AMUI::ListBox(0, 0, 230,(scrHeight/2)+20, background, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

                    bglabel1=new AMUI::Label(0, 0, /*scrWidth-((scrWidth)/18)*/230, 37, optionlist, opt1, 0, smallfont);
                    bglabel2=new AMUI::Label(0, 0, 230, 37, optionlist, opt2, 0, smallfont);
                    bglabel3=new AMUI::Label(0, 0, 230, 37, optionlist, opt3, 0, smallfont);
                    bglabel4=new AMUI::Label(0, 0, 230, 37, optionlist, opt4, 0, smallfont);

                bglabel1->setSkin(checkbox_skin);
                bglabel1->setHorizontalAlignment(AMUI::Label::HA_CENTER);
                bglabel1->setVerticalAlignment(AMUI::Label::VA_CENTER);
                bglabel1->setMultiLine(true);

                bglabel2->setSkin(checkbox_skin);
                bglabel2->setHorizontalAlignment(AMUI::Label::HA_CENTER);
                bglabel2->setVerticalAlignment(AMUI::Label::VA_CENTER);
                bglabel2->setMultiLine(true);

                bglabel3->setSkin(checkbox_skin);
                bglabel3->setHorizontalAlignment(AMUI::Label::HA_CENTER);
                bglabel3->setVerticalAlignment(AMUI::Label::VA_CENTER);
                bglabel3->setMultiLine(true);

                bglabel4->setSkin(checkbox_skin);
                bglabel4->setHorizontalAlignment(AMUI::Label::HA_CENTER);
                bglabel4->setVerticalAlignment(AMUI::Label::VA_CENTER);
                bglabel4->setMultiLine(true);

                optionlist->setDrawBackground(false);

                bglabel1->setSelected(false);
                bglabel2->setSelected(false);
				bglabel3->setSelected(false);
				bglabel4->setSelected(false);

				//optionlist-> setPosition(5, (scrHeight/4)+30);
				//optionlist-> setPosition(5, (scrHeight/4)+30);
				optionlist->  setPosition((scrWidth/2)-115, (scrHeight/2)-40);

              	QNumber_Label = new AMUI::Label(scrWidth/2-20, 2*scrHeight/3+55,scrWidth-80,20,background,QNumber,0,smallfont);

                QNumber_Label->setSkin(NULL);
            	QNumber_Label->setDrawBackground(false);

            answer_back_list=new AMUI::ListBox(0, 0, (scrWidth-10), 32, background, AMUI::ListBox::LBO_HORIZONTAL, AMUI::ListBox::LBA_LINEAR, true);

            back_button=new AMUI::Label(0, 0,(scrWidth/2)+30 , 32, answer_back_list, "Back", 0, smallfont);
            /*final_answer=new AMUI::Label(0, 0, (scrWidth-10)/2, 32, answer_back_list, "Final Answ?", 0, smallfont);


        final_answer->setSkin(bottom_buttons_skin);
        final_answer->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        final_answer->setVerticalAlignment(AMUI::Label::VA_CENTER);
        final_answer->setSelected(true);*/

        back_button->setSkin(bottom_buttons_skin);
        back_button->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        back_button->setVerticalAlignment(AMUI::Label::VA_CENTER);
        back_button->setSelected(false);

       answer_back_list->setDrawBackground(false);


	   answer_back_list-> setPosition(5, (3*scrHeight/4)+scrHeight/8+10);
	   //back_button->setPosition(0,(scrWidth/2)-50);
	   //answer_back_list-> setPosition(scrWidth/4*3-scrWidth/2-40, (3*scrHeight/4)+scrHeight/8);

        layout->add(background);
        this->setMain(layout);
}

PlayScreen::~PlayScreen() {
        delete layout;
        for(int i = 0; i < screens.size(); i++) {
        	delete screens[i];
        	screens[i]=NULL;
        }
        layout=NULL;

        unloadResource(PLAY_IMAGE);
        unloadResource(RES_FONT);
        unloadResource(RES_FONT_SMALLER);
        unloadResource(RES_CHECKBOX_CHECKED);
		unloadResource(RES_CHECKBOX_UNCHECKED);
		unloadResource(RES_BUTTON_ACTIVE);
		unloadResource(RES_BUTTON_INACTIVE);

        if(level==e){
        		unloadResource(EASY);
        }
        else if(level==m){
        		unloadResource(MEDIUM);
        }
        else if(level==h){
        		unloadResource(HARD);
        }
        else if(level==p){
        		unloadResource(PRO);
        }
}


void PlayScreen::keyPressEvent(int keyCode, int nativeCode) {

        switch(keyCode) {
        case MAK_UP:
                optionlist->selectPreviousItem();
             //   selectPreviousItem();
                break;
        case MAK_DOWN:
                optionlist->selectNextItem();
             //   selectNextItem();
                break;
        case MAK_LEFT:
                answer_back_list->selectPreviousItem();
             //   selectPreviousItem();
                break;
        case MAK_RIGHT:
                answer_back_list->selectNextItem();
             //   selectNextItem();
                break;
        case MAK_SOFTLEFT:
                previous->show();
                break;
        case MAK_BACK:
                previous->show();
                break;
        case MAK_SOFTRIGHT:
        	break;
        case MAK_FIRE:
        	buttonAction();
                break;
}

}
void PlayScreen::buttonAction(){

    if(bglabel1->isSelected()){
    	processAnswer();
             bglabel1->setSelected(true);
             bglabel2->setSelected(false);
             bglabel3->setSelected(false);
             bglabel4->setSelected(false);
             //processAnswer();


    }
    if(bglabel2->isSelected()){
    	processAnswer();
             bglabel2->setSelected(true);
             bglabel1->setSelected(false);
             bglabel3->setSelected(false);
             bglabel4->setSelected(false);
            // processAnswer();
    }
    if(bglabel3->isSelected()){
    	processAnswer();
             bglabel3->setSelected(true);
             bglabel1->setSelected(false);
             bglabel2->setSelected(false);
             bglabel4->setSelected(false);
             //processAnswer();
    }
    if(bglabel4->isSelected()){
    	processAnswer();
             bglabel4->setSelected(true);
             bglabel1->setSelected(false);
             bglabel2->setSelected(false);
             bglabel3->setSelected(false);


    }
   /*if(final_answer->isSelected()){

	if(!( bglabel1->isSelected()||bglabel2->isSelected()||bglabel3->isSelected()||bglabel4->isSelected())){
                    //No answer selected..ask user to select a question
                    lprintfln("Choose at least one answer or press BACK.");
                    maAlert("Choose ", "Please Choose one answer or press BACK", "OK", NULL, NULL);
            }
            //Go ahead and get the answer
            else{
            //Get the answer chosen
            	processAnswer();
            }
    }*/
    if(back_button->isSelected()){
            // THIS IS NOW BACK...NOT PREVIOUS
            previous->show();
    }
}

void PlayScreen::selectPreviousItem(){
	lprintfln("Selected: ", "P");
}

void PlayScreen::selectNextItem(){
		lprintfln("Selected: ", "N");
}


void PlayScreen::show() {
	AMUI::Screen::show();
}

void PlayScreen::hide() {
		Screen::hide();
}

void PlayScreen::pointerPressEvent(MAPoint2d point) {
}

void PlayScreen::pointerReleaseEvent(MAPoint2d point) {

        Point p;
                p.set(point.x, point.y);

                if(bglabel1->contains(p)){
                	processAnswer();
                         bglabel1->setSelected(true);
                         bglabel2->setSelected(false);
                         bglabel3->setSelected(false);
                         bglabel4->setSelected(false);
                         lprintfln("Option 1 selected");

                }
                if(bglabel2->contains(p)){
                	processAnswer();
                         bglabel2->setSelected(true);
                         bglabel1->setSelected(false);
                         bglabel3->setSelected(false);
                         bglabel4->setSelected(false);
                }
                if(bglabel3->contains(p)){
                	processAnswer();
                         bglabel3->setSelected(true);
                         bglabel1->setSelected(false);
                         bglabel2->setSelected(false);
                         bglabel4->setSelected(false);
                }
                if(bglabel4->contains(p)){
                	processAnswer();
                         bglabel4->setSelected(true);
                         bglabel1->setSelected(false);
                         bglabel2->setSelected(false);
                         bglabel3->setSelected(false);
                }
               /* if(final_answer->contains(p)){

				if(!( bglabel1->isSelected()||bglabel2->isSelected()||bglabel3->isSelected()||bglabel4->isSelected())){
                                //No answer selected..ask user to select a question
                                lprintfln("Choose at least one answer or press BACK.");
                                maAlert("Choose ", "Please Choose one answer or press BACK", "OK", NULL, NULL);
                        }
                        //Go ahead and get the answer
                        else{
                        //Get the answer chosen
                        	processAnswer();
                        }
                }*/
                if(back_button->contains(p)){
                        // THIS IS NOW BACK...NOT PREVIOUS
                        previous->show();
                }
                        }


String PlayScreen::LoadTrivia(int new_position, String triviapart){
        String to_return="";

        current_trivia = doc->rootNode->find("trivia")[new_position];
        current_part=current_trivia->find(triviapart)[0]->getInnerText();
        to_return =current_part.c_str();

        lprintfln("New Var: %s", to_return.c_str());
        return to_return;
}
void PlayScreen::processAnswer(){

    String chosen_answer="";
    if( bglabel1->isSelected()){
            chosen_answer="1";
    }
    else if( bglabel2->isSelected()){
            chosen_answer="2";
    }
    else if( bglabel3->isSelected()){
            chosen_answer="3";
    }
    else{
            chosen_answer="4";
    }

    //Get better result to tell user
    String the_answer="";
    if(Canswer=="1"){
            the_answer=opt1;
    }
    else if(Canswer=="2"){
            the_answer=opt2;
    }
    else if(Canswer=="3"){
            the_answer=opt3;
    }
    else{
            the_answer=opt4;
    }

    //Test the answer
    if(chosen_answer==Canswer){

    		new_position=new_position+1;

			if(new_position==15){
			//	Screen::hide();
				lprintfln("Going to badge screen!"); //send congrats there and level..to the BADGE SCREEN
				MyBadgesScreen* badge_screen=new MyBadgesScreen(this, level);

				badge_screen->writeStore(level,level); //save badge status...
				String test=badge_screen->readStore(level);

				lprintfln("Test: Play%s", test.c_str());
				String level_unlocked="You have unlocked the "+test+" BADGE";
				maAlert("Congratulations!!",level_unlocked.c_str(), "OK", NULL, NULL);
				badge_screen->show();
				}
			else{//there is another question

            lprintfln("You are right!. The answer is: %s", the_answer.c_str());
            maAlert("CORRECT! ", "Well done...", "Next Question", NULL, NULL);

			lprintfln("New Position: %d", new_position);

			int label_psn=new_position+1;
			char* posn=itoa(label_psn, cBuffer, 10);
			lprintfln(posn);
			char* tail="/15";

			char result[100];
			strcpy(result, posn);
			strcat(result, tail);

			QNumber=result;
			QNumber_Label->setCaption(QNumber);

			Cquestion=LoadTrivia(new_position, "question");
			opt1=LoadTrivia(new_position, "option1");
			opt2=LoadTrivia(new_position, "option2");
			opt3=LoadTrivia(new_position, "option3");
			opt4=LoadTrivia(new_position, "option4");
			Canswer=LoadTrivia(new_position, "answer");

			question->setCaption(Cquestion);
			bglabel1->setCaption(opt1);
			bglabel2->setCaption(opt2);
			bglabel3->setCaption(opt3);
			bglabel4->setCaption(opt4);
			}
            }

    else{ //        when answer is wrong

            lprintfln("Wrong. The right answer is %s", the_answer.c_str());
            String alertanswer="The right answer is: "+ the_answer;
            maAlert("Game Over! ", alertanswer.c_str(), "Try Again", NULL, NULL);
            lprintfln("game over");
            previous->show();
    }

	 bglabel1->setSelected(false);
	 bglabel2->setSelected(false);
	 bglabel3->setSelected(false);
	 bglabel4->setSelected(false);
}
