#ifndef SPLASHSCREEN_H_
#define SPLASHSCREEN_H_

#include "MainScreen.h"

using namespace MAUI;
using namespace MAUtil;
AMUI::Image* splash;

class SplashScreen : public AMUI::Screen, public TimerListener
{
public:
SplashScreen(MAHandle imageHandle)
{
	 splash = new AMUI::Image(0, 0, 240, 320, NULL, false, false, imageHandle);
	// splash->setAllowResize(AMUI::Widget::ALLOW_RESIZE_BOTH);
	 splash->setScaleImage(true);

this->setMain(splash);
Environment::getEnvironment().addTimer(this, 2000, 1);
}
~SplashScreen(){
	delete splash;
}

//TimerListener
void runTimerEvent()
{
showNextScreen();
}

//KeyEvents
void keyReleaseEvent(int keyCode, int nativeCode)
{
showNextScreen();
}
private:
AMUI::Screen* mainScreen;

void showNextScreen()
{
//CALL NEXT SCREEN HERE
	mainScreen = new MainScreen();
	mainScreen->show();
}

};
#endif /* SPLASHSCREEN_H_ */
