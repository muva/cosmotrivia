/*
Copyright (C) 2011 MoSync AB

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License,
version 2, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
MA 02110-1301, USA.
*/

#include <AMUI/Label.h>
#include <AMUI/Layout.h>
#include <AMUI/ListBox.h>
#include "Util.h"
#include <MAHeaders.h>
#include <AMUI/Image.h>

AMUI::Font *gFont;
AMUI::WidgetSkin *gSkin;
MAExtent screenSize=maGetScrSize();
int scrWidth=EXTENT_X(screenSize);
int scrHeight=EXTENT_Y(screenSize);


/*
void setLabelPadding(AMUI::Widget *w) {
	w->setPaddingLeft(PADDING);
	w->setPaddingBottom(PADDING);
	w->setPaddingRight(PADDING);
	w->setPaddingTop(PADDING);
}


AMUI::Label* createLabel(const char *str, int height) {
	AMUI::Label *label;
	label = new AMUI::Label(0,0, scrWidth-PADDING*2, height, NULL, str, 0, gFont);
	label->setSkin(gSkin);
	setLabelPadding(label);
	return label;
}

	AMUI::Widget* createSoftKeyBar(int height, const char *left, const char *right) {
	AMUI::Layout *layout = new AMUI::Layout(0, 0, scrWidth, height, NULL, 2, 1);
	AMUI::Label *label;

	label = new AMUI::Label(0,0, scrWidth/2, height, NULL, left, 0, gFont);
	label->setHorizontalAlignment(AMUI::Label::HA_LEFT);
	label->setDrawBackground(false);
	setLabelPadding(label);
	layout->add(label);

	label = new AMUI::Label(0,0, scrWidth/2, height, NULL, right, 0, gFont);
	label->setDrawBackground(false);
	label->setHorizontalAlignment(AMUI::Label::HA_RIGHT);
	setLabelPadding(label);
	layout->add(label);

	return layout;
}


// first child is listbox
	AMUI::Layout* createMainLayout(const char *left, const char *right) {
	AMUI::Layout *mainLayout = new AMUI::Layout(0, 0, scrWidth, scrHeight, NULL, 1, 2);
	AMUI::Image* image = new AMUI::Image(0, 0, 100, 100, NULL, true, true, IMAGE);
	mainLayout->add(image);
	mainLayout->getChildren()[0];

//	AMUI::Widget *softKeys = createSoftKeyBar(30, left, right);

	AMUI::ListBox* myBox = new AMUI::ListBox(0, 0, scrWidth, scrHeight-softKeys->getHeight(), mainLayout, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

	myBox->setSkin(NULL);
	myBox->setPaddingLeft(5);
	myBox->setPaddingRight(5);
	myBox->setPaddingTop(5);
	myBox->setPaddingBottom(5);


	mainLayout->add(softKeys);

	mainLayout->setSkin(NULL);
	mainLayout->setDrawBackground(true);
	mainLayout->setBackgroundColor(NULL);

	return mainLayout;
}
*/
