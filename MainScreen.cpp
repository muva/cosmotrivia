/*
 * Since we have defined all the methods we will need in this class, now it is time to define
 * what the main screen will look like. We also include any other attached screens. This is how it is done.
 *
 */
#include "MainScreen.h"//This must be included in order to use the defined set of methods in the header
//#include "PlayScreen.h"//From here and below are the screens
#include "CategoryScreen.h"
#include "AboutScreen.h"
#include "SplashScreen.h"
#include "MyBadgesScreen.h"
#include "MAHeaders.h"//Now this is highly required so that to use the resources defined in res.lst.
#include "Util.h"//This pretty much defines the screen features. We will get there.
#include <conprint.h>
#include <ResCompiler/ResCompiler.h>

MyMoblet *moblet;

MainScreen::MainScreen() {

    loadResource(SELECTED_MAIN);
    loadResource(UNSELECTED_MAIN);
    loadResource(RES_FONT);
    loadResource(RES_FONT_SMALLER);
    loadResource(SPLASH);


	screens.add(new CategoryScreen(this));
	screens.add(new AboutScreen(this));


	layout = new AMUI::Layout(0, 0, scrWidth, scrHeight, NULL, 1, 2);

	background = new AMUI::Image(0, 0, scrWidth, scrHeight, NULL, false, false, IMAGE);
	background->setAllowResize(AMUI::Widget::ALLOW_RESIZE_NONE);
	background->setScaleImage(true);

//	background->setSkin(gSkin);
	background->setDrawBackground(true);
	layout->add(background);

	button_skin = new AMUI::WidgetSkin(
			SELECTED_MAIN,   // Image for selected state.
			UNSELECTED_MAIN, // Image for unselected state.
			0, // X coordinate for start of center patch.
			0, // X coordinate for end of center patch.
			0, // Y coordinate for start of center patch.
			0, // Y coordinate for end of center patch.
			true,  // Is selected image transparent?
			true); // Is unselected image transparent?

		font = new AMUI::Font(RES_FONT);
		mainMenu = new AMUI::ListBox(0, 0, 230, (50 * 5), background, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

		play = new AMUI::Label(0, 0, 230, 50, mainMenu, "Play",0, font);
		badge = new AMUI::Label(0, 0, 230, 50, mainMenu, "My Badges", 0, font);
		//fame = new AMUI::Label(0, 0, 230, 50, mainMenu, "Hall of Fame", 0, font); //TO BE NULLED LATER
		about = new AMUI::Label(0, 0, 230, 50, mainMenu,"About", 0, font);
		quit = new AMUI::Label(0, 0, 230, 50, mainMenu, "Quit",	0, font);


	play->setSkin(button_skin);
	about->setSkin(button_skin);
	badge->setSkin(button_skin);
	//fame->setSkin(button_skin);
	quit->setSkin(button_skin);

	play->setHorizontalAlignment(AMUI::Label::HA_CENTER);
	play->setVerticalAlignment(AMUI::Label::VA_CENTER);

	about->setHorizontalAlignment(AMUI::Label::HA_CENTER);
	about->setVerticalAlignment(AMUI::Label::VA_CENTER);

	badge->setHorizontalAlignment(AMUI::Label::HA_CENTER);
	badge->setVerticalAlignment(AMUI::Label::VA_CENTER);

	//fame->setHorizontalAlignment(AMUI::Label::HA_CENTER);
	//fame->setVerticalAlignment(AMUI::Label::VA_CENTER);

	quit->setHorizontalAlignment(AMUI::Label::HA_CENTER);
	quit->setVerticalAlignment(AMUI::Label::VA_CENTER);

	mainMenu->setDrawBackground(false);
	mainMenu-> setPosition((scrWidth/2)-115, (scrHeight/4)-20);

	this->setMain(layout);
}
MainScreen::~MainScreen() {
	delete layout;
	for (int i = 0; i < screens.size(); i++) {
		delete screens[i];
		screens[i] = NULL;
	}
	layout = NULL;

    unloadResource(SELECTED_MAIN);
    unloadResource(UNSELECTED_MAIN);
    unloadResource(RES_FONT);
    unloadResource(RES_FONT_SMALLER);
    unloadResource(SPLASH);
}

void MainScreen::keyPressEvent(int keyCode, int nativeCode) {

	switch (keyCode) {
	case MAK_UP:
		mainMenu->selectPreviousItem();
		break;
	case MAK_DOWN:
		mainMenu->selectNextItem();
		break;
	case MAK_SOFTLEFT:
		// Exit the application.
		maExit(0);
		break;
	case MAK_BACK:
		maExit(0);
		break;
	case MAK_SOFTRIGHT:
	case MAK_FIRE:
		buttonAction();
		break;
	}

}
void MainScreen::buttonAction() {
	if (play->isSelected()) {
		Screen::hide();
		screens[0]->show();
	}
	if (about->isSelected()) {
		screens[1]->show();
	}
	if (badge->isSelected()) {
		//     	screens.add(new MyBadgesScreen(this, "menu"));
		//     	screens[2]->show();
		MyBadgesScreen* badge = new MyBadgesScreen(this, "menu");
		badge->show();
	}
	/*if (fame->isSelected()) {
		maPlatformRequest("http://kenyatrivia.com/hall.html");
	}*/
	if (quit->isSelected()) {
		maExit(0);
	}
}

void MainScreen::pointerPressEvent(MAPoint2d point) {
}

void MainScreen::pointerReleaseEvent(MAPoint2d point) {
	Point p;
	p.set(point.x, point.y);
	if (play->contains(p)) {
		Screen::hide();
		screens[0]->show();

	}
	if (about->contains(p)) {
		screens[1]->show();
	}
	if (badge->contains(p)) {
		MyBadgesScreen* badge = new MyBadgesScreen(this, "menu");
		badge->show();
	}
	/*if (fame->contains(p)) {
		//screens[3]->show();
		maPlatformRequest("http://kenyatrivia.com/hall.html");
		//maPlatformRequest("sms:0723220235");
	}*/
	if (quit->contains(p)) {
		maExit(0);
	}
}

void MyMoblet::keyPressEvent(int keyCode, int nativeCode) {
}

void MyMoblet::keyReleaseEvent(int keyCode, int nativeCode) {
}

void MyMoblet::closeEvent() {
	// do destruction here
	delete mainScreen;
	delete splash_Brand_Screen;
}

MyMoblet::MyMoblet() {

	gFont = new AMUI::Font(RES_FONT);
//	Engine& engine = Engine::getSingleton();
//	engine.setDefaultFont(gFont);
	//engine.setDefaultSkin(gSkin);

	MAExtent screenSize = maGetScrSize();
	scrWidth = EXTENT_X(screenSize);
	scrHeight = EXTENT_Y(screenSize);



	mainScreen = new MainScreen();
	splash_Brand_Screen = new SplashScreen(SPLASH);
	splash_Brand_Screen->show();
}

extern "C" int MAMain() {
	moblet = new MyMoblet();
	MyMoblet::run(moblet);
	return 0;
}

