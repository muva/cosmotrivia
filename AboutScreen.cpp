#include "MAHeaders.h"
#include "AboutScreen.h"
#include "Util.h"
#include "MAHeaders.h"
#include <AMUI/ListBox.h>
#include <ResCompiler/ResCompiler.h>
#include <AMUI/Image.h>
#include <AMUI/StackLayout.h>


AboutScreen::AboutScreen(AMUI::Screen *previous) : previous(previous) {

    loadResource(ABOUT);
    loadResource(RES_FONT);
    loadResource(RES_FONT_SMALLER);
    loadResource(ABOUT_BUTTON_ACTIVE);
    loadResource(ABOUT_BUTTON_INACTIVE);

	mainLayout = new AMUI::StackLayout(0, 0, scrWidth, scrHeight, NULL, AMUI::StackLayout::SL_VERTICAL, AMUI::StackLayout::SL_STRETCH);//pos, width, height

	about_background = new AMUI::Image(0,0,scrWidth,scrHeight,NULL,false,false,ABOUT);
	about_background->setScaleImage(true);

    button_skin = new AMUI::WidgetSkin(
    				ABOUT_BUTTON_ACTIVE,   // Image for selected state.
                    ABOUT_BUTTON_INACTIVE, // Image for unselected state.
                    0, // X coordinate for start of center patch.
                    0, // X coordinate for end of center patch.
                    0, // Y coordinate for start of center patch.
                    0, // Y coordinate for end of center patch.
                    false,  // Is selected image transparent?
                    false); // Is unselected image transparent?

    about_font = new AMUI::Font(RES_FONT_SMALLER);
	aboutlist=new AMUI::ListBox(0, 0, 230 ,130, about_background, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

	back=new AMUI::Label(0, 0, 230, 65, aboutlist, "BACK", 0, about_font);
	exit=new AMUI::Label(0, 0, 230, 65, aboutlist, "EXIT", 0, about_font);


			back->setSkin(button_skin);
			back->setHorizontalAlignment(AMUI::Label::HA_CENTER);
			back->setVerticalAlignment(AMUI::Label::VA_CENTER);

			exit->setSkin(button_skin);
			exit->setHorizontalAlignment(AMUI::Label::HA_CENTER);
			exit->setVerticalAlignment(AMUI::Label::VA_CENTER);

			aboutlist->setDrawBackground(false);

			aboutlist-> setPosition(scrWidth/2-115, (2*scrHeight/3)-30);
			mainLayout->add(about_background);
			this->setMain(mainLayout);
}

AboutScreen::~AboutScreen() {
	delete mainLayout;
	//for(int i = 0; i < screens.size(); i++) delete screens[i];
	mainLayout=NULL;
	unloadResource(ABOUT);
	unloadResource(ABOUT_BUTTON_ACTIVE);
	unloadResource(ABOUT_BUTTON_INACTIVE);
	unloadResource(RES_FONT);
	unloadResource(RES_FONT_SMALLER);
}

void AboutScreen::keyPressEvent(int keyCode, int nativeCode) {
	switch(keyCode)
	{
	case MAK_SOFTRIGHT:
		previous->show();
		break;
	case MAK_UP:
		aboutlist->selectPreviousItem();
		break;
	case MAK_DOWN:
		aboutlist->selectNextItem();
		break;
	case MAK_BACK:
		previous->show();
		break;
	case MAK_FIRE:
		buttonAction();
		break;
}
}
void AboutScreen::buttonAction(){
        if(back->isSelected()){
                Screen::hide();
                previous->show();
        }
        if(exit->isSelected()){
        	maExit(0);
        }
}

void AboutScreen::pointerPressEvent(MAPoint2d point) {

}

void AboutScreen::pointerReleaseEvent(MAPoint2d point) {
    Point p;
            p.set(point.x, point.y);

            if(back->contains(p)){
            	previous->show();
            }
            if(exit->contains(p)){
            	maExit(0);
			}
	}

