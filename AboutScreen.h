#ifndef _IMAGESCREEN_H_
#define _IMAGESCREEN_H_

#include <AMUI/Screen.h>
#include <AMUI/StackLayout.h>
#include <AMUI/ListBox.h>
#include <MAUtil/Moblet.h>
#include <AMUI/Label.h>
#include <ma.h>
#include "Util.h"
#include <AMUI/Image.h>
#include <AMUI/Widget.h>
#include "MAHeaders.h"
#include <AMUI/Image.h>
#include <AMUI/ListBox.h>


//using namespace MAUI;
using namespace MAUtil;

class AboutScreen : public AMUI::Screen {
public:
	AboutScreen(AMUI::Screen *previous);
	~AboutScreen();
	void keyPressEvent(int keyCode, int nativeCode);
	void pointerPressEvent(MAPoint2d point);
	void pointerReleaseEvent(MAPoint2d point);
	void buttonAction();

private:
	//Vector<Screen*> screens;
	AMUI::Screen *previous;
	AMUI::StackLayout *mainLayout;
	AMUI::Image* about_background;
	AMUI::WidgetSkin* button_skin;
	AMUI::ListBox* aboutlist;
	AMUI::Label* back;
	AMUI::Label* exit;
	AMUI::Font* about_font;

};

#endif	//_IMAGESCREEN_H_
