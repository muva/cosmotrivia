#include <MAUtil/Connection.h>
#include "MyBadgesScreen.h"
#include "MAHeaders.h"
#include "Util.h"
#include <MAUI/Widget.h>
#include <conprint.h>
#include <mastdlib.h>
#include <ResCompiler/ResCompiler.h>
#define CONNECTION_BUFFER_SIZE 1024

MyBadgesScreen::MyBadgesScreen(AMUI::Screen *previous, String the_level) : previous(previous), mHttp(this){

    loadResource(ABOUT_BUTTON_ACTIVE);
    loadResource(ABOUT_BUTTON_INACTIVE);
    loadResource(RES_FONT);
    loadResource(RES_FONT_SMALLER);
    loadResource(BADGEBG);
    loadResource(EASY_BADGE_LOCKED);
    loadResource(EASY_BADGE_UNLOCKED);
    loadResource(MEDIUM_BADGE_LOCKED);
    loadResource(MEDIUM_BADGE_UNLOCKED);
    loadResource(HARD_BADGE_LOCKED);
    loadResource(HARD_BADGE_UNLOCKED);
    loadResource(PRO_BADGE_LOCKED);
    loadResource(PRO_BADGE_UNLOCKED);


	screens.add(new MainScreen());

	level=the_level.c_str();


	//handle incoming strings and if coming from playscreen...save it as a completed level if not, ignore.
		if(level=="menu"){
			lprintfln("menu"); //send congrats there and level..to the BADGE SCREEN
		}

		else{
			writeStore(level,level);
		}


		//load images based on what is on store...if a level was completed, use unlocked image...else, use a locked image
		//EASY LEVEL
		String easy=readStore("easy");
		lprintfln("Easy: %s", easy.c_str());
		if(easy==""){
			image1 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, EASY_BADGE_LOCKED);
		}
		else{
			image1 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, EASY_BADGE_UNLOCKED);
		}
		//MEDIUM LEVEL
		String medium=readStore("medium");
		lprintfln("Medium: %s", medium.c_str());
		if(medium==""){
			image2 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, MEDIUM_BADGE_LOCKED);
		}
		else{
			image2 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, MEDIUM_BADGE_UNLOCKED);
		}
		//HARD LEVEL
		String hard=readStore("hard");
		if(hard==""){
			image3 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, HARD_BADGE_LOCKED);
		}
		else{
			image3 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, HARD_BADGE_UNLOCKED);
		}
		//PRO LEVEL
		String pro=readStore("pro");
		if(pro==""){
			image4 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, PRO_BADGE_LOCKED);
		}
		else{
			image4 = new AMUI::Image(0, 0, (scrWidth-10)/2, scrHeight/3, NULL, true, true, PRO_BADGE_UNLOCKED);
		}

		//PROCEED TO CREATE LAYOUTS
        mainLayout = new AMUI::Layout(0, 0, scrWidth, scrHeight, NULL,1, 2);
        badgeSrcBackGround=new AMUI::Image(0,0,scrWidth,scrHeight,NULL,false,false,BADGEBG);
    	badgeSrcBackGround->setAllowResize(AMUI::Widget::ALLOW_RESIZE_NONE);
    	badgeSrcBackGround->setScaleImage(true);
        badge1_layout=new AMUI::Layout(5, 0, scrWidth-10,2*scrHeight/3, badgeSrcBackGround, 2, 2);

    badge1_layout->add(image1);
    badge1_layout->add(image2);
    badge1_layout->add(image3);
    badge1_layout->add(image4);
    badge1_layout->setPaddingTop(5);

    badge1_layout-> setPosition(scrWidth/2-110,0);

    button_skin = new AMUI::WidgetSkin(
				ABOUT_BUTTON_ACTIVE,   // AMUI::Image for selected state.
				ABOUT_BUTTON_INACTIVE, // AMUI::Image for unselected state.
						0, // X coordinate for start of center patch.
						0, // X coordinate for end of center patch.
				0, // Y coordinate for start of center patch.
				0, // Y coordinate for end of center patch.
				true,  // Is selected image transparent?
				true); // Is unselected image transparent?

         badge_font = new AMUI::Font(RES_FONT);
    	 badge_list=new AMUI::ListBox(0, 0, 230, 65*2, badgeSrcBackGround, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

    	 back=new AMUI::Label(0, 0, 230, 65, badge_list, "BACK", 0, badge_font);
    	 reset=new AMUI::Label(0, 0, 230, 65, badge_list, "RESET", 0, badge_font);
    	 exit=new AMUI::Label(0, 0, 230, 65, NULL, "EXIT", 0, badge_font);

				back->setSkin(button_skin);
				back->setHorizontalAlignment(AMUI::Label::HA_CENTER);
				back->setVerticalAlignment(AMUI::Label::VA_CENTER);
				back->setMultiLine(true);

				reset->setSkin(button_skin);
				reset->setHorizontalAlignment(AMUI::Label::HA_CENTER);
				reset->setVerticalAlignment(AMUI::Label::VA_CENTER);
				reset->setMultiLine(true);


				exit->setSkin(button_skin);
				exit->setHorizontalAlignment(AMUI::Label::HA_CENTER);
				exit->setVerticalAlignment(AMUI::Label::VA_CENTER);
				exit->setMultiLine(true);

				badge_list->setDrawBackground(false);
				badge_list-> setPosition(scrWidth/2-115, 2*scrHeight/3-30);

				mainLayout->add(badgeSrcBackGround);
				this->setMain(mainLayout);
}

MyBadgesScreen::~MyBadgesScreen() {
    delete mainLayout;
    for(int i = 0; i < screens.size(); i++) {
    	delete screens[i];
    	screens[i]=NULL;
    }
    mainLayout=NULL;

    unloadResource(ABOUT_BUTTON_ACTIVE);
    unloadResource(ABOUT_BUTTON_INACTIVE);
    unloadResource(RES_FONT);
    unloadResource(RES_FONT_SMALLER);
    unloadResource(BADGEBG);
    unloadResource(EASY_BADGE_LOCKED);
    unloadResource(EASY_BADGE_UNLOCKED);
    unloadResource(MEDIUM_BADGE_LOCKED);
    unloadResource(MEDIUM_BADGE_UNLOCKED);
    unloadResource(HARD_BADGE_LOCKED);
    unloadResource(HARD_BADGE_UNLOCKED);
    unloadResource(PRO_BADGE_LOCKED);
    unloadResource(PRO_BADGE_UNLOCKED);
}

void MyBadgesScreen::keyPressEvent(int keyCode, int nativeCode) {
        switch(keyCode)
                {
                case MAK_SOFTRIGHT:
                        previous->show();
                        break;
                case MAK_UP:
                        badge_list->selectPreviousItem();
                        break;
                case MAK_DOWN:
                        badge_list->selectNextItem();
                        break;
                case MAK_BACK:
                	buttonAction();
                	//screens[0]->show();
                        break;
                case MAK_FIRE:
                        buttonAction();
                        break;
        }
}

void MyBadgesScreen::checkBadge2Delete(){
	String easy=readStore("easy");
	if(easy==""){

	}else{
		deleteStore(easy);
	}

	String medium=readStore("medium");
		if(medium==""){

		}else{
			deleteStore(medium);
		}

		String hard=readStore("hard");
			if(hard==""){

			}else{
				deleteStore(hard);
			}

			String pro=readStore("pro");
				if(pro==""){

				}else{
					deleteStore(pro);
				}
}
void MyBadgesScreen::buttonAction(){
        if(back->isSelected()){
               // screens[0]->show();
        	scrn=new MainScreen();
        	scrn->show();
        }
        if(exit->isSelected()){
                maExit(0);
        }
        if(reset->isSelected()){
        	lprintfln("RESET COMPLETE");
        				checkBadge2Delete();
        				screens[0]->show();
            }
}

void MyBadgesScreen::pointerPressEvent(MAPoint2d point) {

}

void MyBadgesScreen::pointerReleaseEvent(MAPoint2d point) {
         Point p;
		p.set(point.x, point.y);

		if(back->contains(p)){
			//screens[0]->show();

			scrn=new MainScreen();
						scrn->show();
		}
		if(exit->contains(p)){
			maExit(0);
        }
		if (reset->contains(p)){
			lprintfln("RESET COMPLETE");
			checkBadge2Delete();
			screens[0]->show();

		}
		if(image1->contains(p)){
			 scr=new PlayScreen(this, "easy");
			scr->show();
		 }
		if(image2->contains(p)){
			scr=new PlayScreen(this, "medium");
			scr->show();
		}
		if(image3->contains(p)){
			 scr=new PlayScreen(this, "hard");
			 scr->show();
		}
		if(image4->contains(p)){
			scr=new PlayScreen(this, "pro");
			scr->show();
		}
	/*
	//DUMMY TESTING USING IMAGE-2...NOT ACTUAL IMPLEMENTATION...BADGES SHUD DO NOTHING..
        if(image2->contains(p)){
                String mydata="name=Evans%20Ndegwa&phone=0723220235&county=Nakuru&badge=Presidential";
                mIsConnected=true;
                PostFame(mydata);
        }
        */
}

void MyBadgesScreen::PostFame(String data){

             if(mIsConnected){
            	mData = data;
                mHttp.create("http://kenyatrivia.com/famous/famous.php", HTTP_POST);
                mHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
                //mHttp.setRequestHeader("User-Agent","Profile/MIDP-2.0 Configuration/CLDC-1.0");
                //mHttp.setRequestHeader("Content-Length", buffer);
                //mHttp.setRequestHeader("Content-Language","fr-FR");
                mHttp.write(mData.c_str(),mData.length());
                }
}
void MyBadgesScreen::connWriteFinished(Connection* conn, int result)
{
    lprintfln("Write finished");
    mHttp.finish();
    lprintfln("writeFinished result: %i\n\n", result);
    mIsConnected=false;
}
void MyBadgesScreen::httpFinished(HttpConnection *conn, int result){
        //Listen for response here
        if(result == 200)
             {
                 lprintfln("httpFinished result: %i\n\n", result);
                 mIsConnected=false;
                 maPlatformRequest("http://kenyatrivia.com/hall.html"); //redirect user to hall of fame in website
             }
        else{
                lprintfln("No Results: ");
                lprintfln("httpFinished-2 result: %i\n\n", result);
                mIsConnected=false;
        }
}

void MyBadgesScreen::connectFinished(Connection* conn, int result) {
        lprintfln("connetFinished result: %i\n\n", result);
        lprintfln("yes");
}
void MyBadgesScreen::connRecvFinished(Connection* conn, int result)
{
        lprintfln("revcFinished result: %i\n\n", result);
    lprintfln("Received data");

    if(result > 0)
    {
        // Get some more
        lprintfln("here");
        memset(buffer, 0, 256);
        mHttp.recv(buffer, 255);
        mIsConnected=false;
    }
    else
    {
        lprintfln("Finished");
        mHttp.close();
        mIsConnected=false;
    }

}
void MyBadgesScreen::connReadFinished(Connection* conn, int result) {
                if(result >= 0)
            lprintfln("connReadFinished %i\n", result);
            else
            lprintfln("connection error %i\n", result);
            mHttp.close();
            mIsConnected=false;

}
void MyBadgesScreen::writeStore(const String& name, const String& data) {
    MAHandle dataHandle = maCreatePlaceholder();
    maCreateData(dataHandle, data.length());
    maWriteData(dataHandle, data.c_str(), 0, data.length());

    MAHandle myStore = maOpenStore(name.c_str(), MAS_CREATE_IF_NECESSARY);
    if (myStore > 0) {
        maWriteStore(myStore, dataHandle);
        maCloseStore(myStore, 0);
    }
    //Delete the data
    maDestroyPlaceholder(dataHandle);
}

String MyBadgesScreen::readStore(const String& name) {
    String data = "";
    MAHandle myData = maCreatePlaceholder();
    MAHandle myStore = maOpenStore(name.c_str(), 0);
    if (myStore != STERR_NONEXISTENT) {
        // The store exists, so we can read from it.
        int result = maReadStore(myStore, myData);
        if (result == RES_OUT_OF_MEMORY) {
            return "empty";
        }
        char tempData[maGetDataSize(myData) + 1];
        memset(tempData, 0, maGetDataSize(myData) + 1);
        maReadData(myData, &tempData, 0, maGetDataSize(myData));
        data = tempData;

    }
    return data;

}
void MyBadgesScreen::deleteStore(const String& name){
	MAHandle myStore = maOpenStore(name.c_str(), 0);
	 if(myStore != STERR_NONEXISTENT)
	    {
	        // use  2 delete the store.
	        maCloseStore(myStore, 1);
	    }

}
