/*
 * CategoryScreen.h
 *
 *  Created on: May 14, 2013
 *      Author: user
 */

#ifndef CATEGORYSCREEN_H_
#define CATEGORYSCREEN_H_

#include <ma.h>
#include <AMUI/Screen.h>
#include <AMUI/Engine.h>
#include <AMUI/Font.h>
#include <AMUI/Label.h>
#include <AMUI/ListBox.h>
#include <AMUI/Image.h>
#include <AMUI/Layout.h>
#include <AMUI/EditBox.h>
#include <MAUtil/Moblet.h>
#include <Ads/Banner.h>


//using namespace MAUI;
using namespace MAUtil;


/**
 *  The screen class used for demonstrating how to
 *  use \a EditBoxes and their different modes.
 */
class CategoryScreen : public AMUI::Screen{
public:
        /**
         * Constructor
         * Sets up the UI hierarchy for this screen, filling
         * it with a number of \a EditBoxes.
         * @param previous a pointer to the screen to return to
         */
        CategoryScreen(AMUI::Screen *previous);
        /**
         * Destructor
         */
        ~CategoryScreen();
        /**
         * Recieves key presses and performs appropriate interaction
         * with the UI.
         */
        void keyPressEvent(int keyCode, int nativeCode);
        void pointerPressEvent(MAPoint2d point);
        void pointerReleaseEvent(MAPoint2d point);
        void setLevel(String the_level);
        String getLevel();

        void selectPreviousItem();
        void selectNextItem();
        void buttonAction();
        void show();
        void hide();

private:

        Vector<Screen*> screens;
        AMUI::Screen *previous;
        AMUI::ListBox* mainMenu;
        AMUI::Image* background;
        AMUI::Image* listImage;

        AMUI::Label* president;
        AMUI::Label* governors;
        AMUI::Label* senators;
        AMUI::Label* mps;
        AMUI::Label* women_reps;
        AMUI::Label* county_reps;
        AMUI::Label* quit;

        AMUI::Layout* layout;
        AMUI::Layout* categspacer;
        AMUI::Widget *softKeys;
        AMUI::Layout* spacer;
        AMUI::Font* font;

        AMUI::WidgetSkin *button_skin;


};

#endif /* CATEGORYSCREEN_H_ */
