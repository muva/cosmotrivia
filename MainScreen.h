/*This is the header where we are going to define all the methods to be used in the main.cpp class.
 * this is something that we will be doing for each and every class that we will be creating. So lets
 * get to i then
 */
/*Below is the #ifndef, which checks whether the given token has been #defined
 * earlier in the file or in an included file; if not, it includes the code between it and the closing
 * #else or, if no #else is present, #endif statement.
 * #ifndef is often used to make header files idempotent by defining a token once the file has been
 * included and checking that the token was not set at the top of that file.
 */
#ifndef _MAIN_H_//start of #ifndef
#define _MAIN_H_
/*Below we include all the classes that will be used in the main.cpp class. All these methods include methods
 * that will be used in that class. To design this game, we need high graphics and that is where
 * MAUI comes in. MAUI helps us in defining screens and widgets for the user interface. For the game, we
 * will need screens, images, labels, listbox, moblet,font, layout, and engine. So we have to include all
 * these classes here.
 */
#include <ma.h>

//#include <MAUI/Screen.h>
#include <AMUI/Screen.h>
#include <MAUI/Engine.h>
#include <MAUI/Font.h>
#include <AMUI/Label.h>
#include <AMUI/ListBox.h>
#include <AMUI/Image.h>
#include <AMUI/Layout.h>
#include <AMUI/EditBox.h>
#include <MAUtil/Moblet.h>
#include <MAUtil/Connection.h>

#include "Util.h"
/*After including all the classes that we will be needing, we need to specify the namespace that will be
 * using. If you don't understand the use of namespace in cpp, please visit the following website and
 * refresh http://www.cplusplus.com/doc/tutorial/namespaces/. Here we will be using MAUI and MAUtil
 * MAUI is for the User Interface while MAUtil is for the functionality part of the class.
 */
using namespace MAUI;
using namespace MAUtil;

/*
 * We define the only widget that we will be using in the main screen
 * The standard constructor for a widget is new Widget(x position, y position, width, height, parent widget)
 */
/*
 * this is the main screen for the game, we will initialize all the methods that we will be using in
 * main.cpp, which will be the main screen.
 */
//class MainScreen : public Screen, WidgetListener, ItemSelectedListener {
class MainScreen : public AMUI::Screen{
public:
        MainScreen();
        ~MainScreen();

        void keyPressEvent(int keyCode, int nativeCode);//this is a moblet class event for pressing a key
        void pointerPressEvent(MAPoint2d point);//this is a moblet class event for moving the pointer
        void pointerReleaseEvent(MAPoint2d point);//moblet class event for when one releases a press event
        void selectPreviousItem();
        void selectNextItem();
        void buttonAction();

private:
        Vector<Screen*> screens;
        AMUI::ListBox* mainMenu;
        AMUI::Image* background;
        AMUI::Image* listImage;


        AMUI::Label* play;
        AMUI::Label* about;
        AMUI::Label* badge;
        AMUI::Label* fame;
        AMUI::Label* quit;

        AMUI::Layout* layout;
        AMUI::Layout* spacer;
        AMUI::Font* font;
        AMUI::WidgetSkin *button_skin;

};

class MyMoblet : public Moblet  {
public:
        MyMoblet();
        void keyPressEvent(int keyCode, int nativeCode);
        void keyReleaseEvent(int keyCode, int nativeCode);
        void closeEvent();

private:
        AMUI::Screen *mainScreen;
        AMUI::Screen* splash_Brand_Screen;

};

#endif /* end of #endif which concludes the #ifndef _MAIN_H_ */
