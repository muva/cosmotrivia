#include "CategoryScreen.h"
#include "Util.h"
#include "MainScreen.h"
#include "MAHeaders.h"
#include "MainScreen.h"
#include "PlayScreen.h"
#include <conprint.h>
#include <ResCompiler/ResCompiler.h>

MainScreen* scrn;
PlayScreen* scr;


CategoryScreen::CategoryScreen(Screen *previous): previous(previous){

        loadResource(IMAGE);
        loadResource(RES_FONT);
        loadResource(RES_FONT_SMALLER);
        loadResource(SELECTED_LEVEL);
		loadResource(UNSELECTED_LEVEL);



		//screens.add(new PlayScreen(this, "easy"));
        layout=new AMUI::Layout(0, 0, scrWidth, scrHeight, NULL, 1, 2);
        categspacer=new AMUI::Layout(0, 0, scrWidth-30, 15, NULL, 1, 2);
        background = new AMUI::Image(0, 0, scrWidth, scrHeight, NULL, false, false, IMAGE);
    	background->setAllowResize(AMUI::Widget::ALLOW_RESIZE_NONE);
    	background->setScaleImage(true);

//        background->setSkin(gSkin);
        background->setDrawBackground(true);
        layout->add(background);

        font = new AMUI::Font(RES_FONT);
        button_skin = new AMUI::WidgetSkin(
                        SELECTED_LEVEL,   // AMUI::Image for selected state.
                        UNSELECTED_LEVEL, // AMUI::Image for unselected state.
                        0, // X coordinate for start of center patch.
                        0, // X coordinate for end of center patch.
                0, // Y coordinate for start of center patch.
                0, // Y coordinate for end of center patch.
                true,  // Is selected image transparent?
                true); // Is unselected image transparent?


				font = new AMUI::Font(RES_FONT);
				mainMenu=new AMUI::ListBox(0, 0, 230, (50*5), background, AMUI::ListBox::LBO_VERTICAL, AMUI::ListBox::LBA_LINEAR, true);

				president=new AMUI::Label(0, 0, 230, 50, mainMenu,"EASY", 0, font);
				governors=new AMUI::Label(0, 0, 230, 50, mainMenu,"MEDIUM", 0, font);
				senators=new AMUI::Label(0, 0, 230, 50, mainMenu,"HARD", 0, font);
				mps=new AMUI::Label(0, 0, 230, 50, mainMenu, "PRO", 0, font);
				women_reps=new AMUI::Label(0, 0, 230, 50, NULL, "Level-5", 0, font);
				county_reps=new AMUI::Label(0, 0, 230, 50, mainMenu, "Back", 0, font);
				quit=new AMUI::Label(0, 0, 230, 50, NULL, "Quit", 0, font);



        president->setSkin(button_skin);
        governors->setSkin(button_skin);
        senators->setSkin(button_skin);
        mps->setSkin(button_skin);
        women_reps->setSkin(button_skin);
        county_reps->setSkin(button_skin);
        quit->setSkin(button_skin);

        president->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        president->setVerticalAlignment(AMUI::Label::VA_CENTER);

        governors->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        governors->setVerticalAlignment(AMUI::Label::VA_CENTER);

        senators->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        senators->setVerticalAlignment(AMUI::Label::VA_CENTER);

        mps->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        mps->setVerticalAlignment(AMUI::Label::VA_CENTER);

        women_reps->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        women_reps->setVerticalAlignment(AMUI::Label::VA_CENTER);

        county_reps->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        county_reps->setVerticalAlignment(AMUI::Label::VA_CENTER);

        quit->setHorizontalAlignment(AMUI::Label::HA_CENTER);
        quit->setVerticalAlignment(AMUI::Label::VA_CENTER);

        president->setSelected(true);

        mainMenu->setDrawBackground(false);

        mainMenu-> setPosition((scrWidth/2)-115, (scrHeight/4)-20);

        this->setMain(layout);
}

CategoryScreen::~CategoryScreen() {
        delete layout;
        for(int i = 0; i < screens.size(); i++) {
        	delete screens[i];
        	screens[i]=NULL;
        }
        delete scr;
        scr=NULL;
        layout=NULL;

        unloadResource(IMAGE);
        unloadResource(RES_FONT);
        unloadResource(RES_FONT_SMALLER);
        unloadResource(SELECTED_LEVEL);
		unloadResource(UNSELECTED_LEVEL);
}

void CategoryScreen::keyPressEvent(int keyCode, int nativeCode) {

        switch(keyCode) {
        case MAK_UP:
                mainMenu->selectPreviousItem();
                break;
        case MAK_DOWN:
                mainMenu->selectNextItem();
                break;
        case MAK_SOFTLEFT:
                previous->show();
                break;
        case MAK_BACK:
                previous->show();
                break;
        case MAK_SOFTRIGHT:
        case MAK_FIRE:
                buttonAction();
                break;
}

}

        void CategoryScreen::buttonAction(){

                if(president->isSelected()){
                       scr=new PlayScreen(this, "easy");
                       scr->show();
                }
                if(governors->isSelected()){
                        scr=new PlayScreen(this, "medium");
                        scr->show();
                }
                if(senators->isSelected()){
                        scr=new PlayScreen(this, "hard");
                        scr->show();
                }
                if(mps->isSelected()){
                        scr=new PlayScreen(this, "pro");
                        scr->show();
                }
                if(senators->isSelected()){
                        //screens[0]->show();
                        maAlert("Coming soon." , "Thanks for playing...we are working on other levels..", "OK", NULL, NULL);
                }
                if(county_reps->isSelected()){
                        //screens[0]->show();
                        //previous->show();
                	scrn=new MainScreen();
                	        	scrn->show();

                }
                if(quit->isSelected()){
                        maExit(0);
                }

        }
        void CategoryScreen::show() {
                Screen::show();
        }

        void CategoryScreen::hide() {
                Screen::hide();
        }

void CategoryScreen::pointerPressEvent(MAPoint2d point) {
}

void CategoryScreen::pointerReleaseEvent(MAPoint2d point) {
        Point p;
                p.set(point.x, point.y);
                if(president->contains(p)){
                        scr=new PlayScreen(this, "easy");
                        scr->show();
                }

                if(governors->contains(p)){
                       scr=new PlayScreen(this, "medium");
                        scr->show();
                }
                if(senators->contains(p)){
                        scr=new PlayScreen(this, "hard");
                        scr->show();

                }
                if(mps->contains(p)){
                        scr=new PlayScreen(this,"pro");
                        scr->show();

                }
                if(women_reps->contains(p)){
                        maAlert("Coming soon." , "Thanks for playing...we are working on other levels..", "OK", NULL, NULL);
                }
                if(county_reps->contains(p)){
                        //previous->show();
                	scrn=new MainScreen();
                	scrn->show();
                }
                if(quit->contains(p)){
                        maExit(0);
                }
}
